// @flow

class Student {
  name: string

  constructor(name: string) {
    this.name = name
  }

  sayHello() {
    return `Labas, labas! As esu ${this.name}`
  }
}

// module.exports = Student;
export default Student
